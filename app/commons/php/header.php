<?php
$actual_link = basename($_SERVER['PHP_SELF']);
switch ($actual_link) {
  case 'home.php':
      $name = 'HF Database';
    break;
  case 'students.php':
      $name = 'Students Database';
    break;
  case 'groups.php':
      $name = '
            <a href="" onclick="location.reload();" class="breadcrumb">Email Manger</a>
            <a href="" onclick="location.reload();" class="breadcrumb" style="color:#fff !important;">Groups</a>
          ';
    break;
  case 'workplace.php':
      $name = 'Workplace Database';
    break;
  case 'index.php':
      $name = '
                <a href="" onclick="location.reload();" class="breadcrumb">Email Manger</a>
                <a href="" onclick="location.reload();"  class="breadcrumb" style="color:#fff !important;">Compose</a>
              ';
    break;
  case 'view_group.php':
      $name = 'Group View';
    break;
  case 'add_group.php':
      $name = '
            <a href="" onclick="location.reload();" class="breadcrumb">Email Manger</a>
            <a href="" onclick="location.reload();"  class="breadcrumb" style="color:#fff !important;">Add Group</a>
          ';
    break;
  default:
    $name = '';
        break;
}
echo
'<nav>
<div class="nav-wrapper">
 <a  id="link_bar" href="" onclick="location.reload();" class="brand-logo">'.$name.'</a>
 <ul class="right hide-on-med-and-down">
   <li><a id="link_bar" onclick="location.reload();"><i data-position="bottom" data-delay="50" data-tooltip="Refresh page" class="tooltipped material-icons">refresh</i></a></li>
   <li><a id="link_bar" href="../db/home.php"><i data-position="bottom" data-delay="50" data-tooltip="HFDatabase" class="tooltipped material-icons">cloud</i></a></li>
   <li><a id="link_bar" href="../db/students.php"><i data-position="bottom" data-delay="50" data-tooltip="StudentsDatabase" class="tooltipped material-icons">school</i></a></li>
   <li><a id="link_bar" href="../db/workplace.php""><i data-position="bottom" data-delay="50" data-tooltip="Workplaces" class="tooltipped material-icons">work</i></a></li>

   <li><a id="link_bar" href="/sls/app/email/groups.php"><i data-position="bottom" data-delay="50" data-tooltip="Groups" class="tooltipped material-icons">group</i></a></li>

   <li><a id="link_bar" href="/sls/app/email/index.php"><i data-position="bottom" data-delay="50" data-tooltip="Write email" class="tooltipped material-icons">email</i></a></li>
   <li><a id="link_bar" target="_blank" href="https://gitlab.com/francescostella97/sls/blob/master/readme.md"><i data-position="bottom" data-delay="50" data-tooltip="Info" class="tooltipped material-icons">info</i></a></li>

 </ul>
</div>
</nav>
';

?>
