<!DOCTYPE html>
 <html>
   <head>
     <!--Import Google Icon Font-->
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
     <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>

     <!--Let browser know website is optimized for mobile-->
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   </head>

   <body>
     <!--Import jQuery before materialize.js-->
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
     <script>
      var emailList = []
      function removeEmail(id,email){
        //console.log(email);
        var i = emailList.indexOf(id);
        if(i != -1) {
        	emailList.splice(i, 1);
          var formInfo = document.getElementById('info').elements["emails"];
          formInfo.value = emailList;
        }

      }
      function addEmail(id,email){
        // console.log("click here");

        if (emailList.indexOf(id) == -1) {
          var chip = document.createElement('div')
          chip.className = "chip"
          chip.innerHTML = email + '<i onclick="removeEmail(&quot;' + email + '&quot;)" class="close material-icons">close</i>'
          document.getElementById('chipContainer').appendChild(chip)
          console.log(emailList.indexOf(id));
          emailList.push(id)
          var formInfo = document.getElementById('info').elements["emails"];
          formInfo.value = emailList;
          console.log(emailList);
        }


      }
       $(document).ready(function() {
          $('select').material_select();
          $('input#input_text, textarea#textarea1').characterCounter();
          $('.tooltipped').tooltip({delay: 50});
       });


      $(function() {
        var options = {
          url: "emails.json",

          getValue: "label",

          list: {
            match: {
              enabled: true
            }
          }
        };
        console.log(options);
        $("autocomplete").autocomplete(options);
         $('.chips').material_chip();
        });

     </script>


     <?php include '../commons/php/header.php'; ?>

  <div class="row">

    <form id="info" action="src\group\add.php" method="post" class="col s12" style="padding-top:25px;">
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">group</i>
          <input id="icon_prefix" type="text" class="validate" name="group_name">
          <label for="icon_prefix">Group Name</label>
        </div>

      </div>
      <div class="row input-field col s6">

        <ul class="collection with-header nav">
         <li class="collection-header"><h4>Choose Group's Components</h4></li>

         <?php
           include '../commons/php/db_connection.php';
           $connection = OpenCon();
           $sql = 'SELECT id,email FROM `family` WHERE email != "" ORDER BY `family`.`email` ASC';
           $result = mysqli_query($connection, $sql);
           $array = array();

           while($row = mysqli_fetch_assoc($result)) {
               //$array[] = array('key' => $row['id'],'name' => $row['email']);
               print '<li class="collection-item"><div>'.$row['email'].'<a  onclick="addEmail(&quot;'.$row['id'].'&quot;,&quot;'.$row['email'].'&quot;)" href="#!" class="secondary-content"><i class="material-icons">send</i></a></div></li>';
           }
         ?>
       </ul>

      </div>
      <div class="row input-field col s6" id="chipContainer">

      </div>





      <div class="fixed-action-btn ">
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
          <i class="material-icons right">send</i>
      </button>
      </div>





      <input type="hidden" name="emails" value="" />
    </form>
  </div>

   </body>
 </html>
