<!DOCTYPE html>
 <html>
   <head>
     <!--Import Google Icon Font-->
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
     <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>

     <!--Let browser know website is optimized for mobile-->
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   </head>

   <body>
     <!--Import jQuery before materialize.js-->
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
     <script>
      var emailList = []
      function removeEmail(email){
        //console.log(email);
        var i = emailList.indexOf(email);
        if(i != -1) {
        	emailList.splice(i, 1);
        }
      }
      function addEmail(email){
        // console.log("click here");
        var chip = document.createElement('div')
        chip.className = "chip"
        chip.innerHTML = email + '<i onclick="removeEmail(&quot;' + email + '&quot;)" class="close material-icons">close</i>'
        document.getElementById('chipContainer').appendChild(chip)
        emailList.push(email)
      }
       $(document).ready(function() {
          $('select').material_select();
          $('input#input_text, textarea#textarea1').characterCounter();
          $('.tooltipped').tooltip({delay: 50});
       });


      $(function() {
        var options = {
          url: "emails.json",

          getValue: "label",

          list: {
            match: {
              enabled: true
            }
          }
        };
        console.log(options);
        $("autocomplete").autocomplete(options);
         $('.chips').material_chip();
        });

     </script>


     <?php include '../commons/php/header.php'; ?>
     <div class="row" style="padding-left:15px;">


  <div class="row input-field col s6">
    <div class="collection">

      <h4>Groups</h4>
      <?php
        include '../commons/php/db_connection.php';
        $connection = OpenCon();
        $sql = 'SELECT id,name FROM `hfgroup` ORDER BY `hfgroup`.`name` ASC';
        $result = mysqli_query($connection, $sql);
        $array = array();

        while($row = mysqli_fetch_assoc($result)) {
            //$array[] = array('key' => $row['id'],'name' => $row['email']);
            print '<a href="view_group.php?id='.$row['id'].'" class="collection-item">'.$row['name'].'</a>';
            //print '<li class="collection-item"><div>'.$row['email'].'<a  onclick="addEmail(&quot;'.$row['email'].'&quot;)" href="#!" class="secondary-content"><i class="material-icons">send</i></a></div></li>';
        }
      ?>

      </div>

		<div class="fixed-action-btn">
        <a href="add_group.php" class="btn-floating btn-large yellow">
          <i data-position="left" data-delay="50" data-tooltip="Add group" class="tooltipped large material-icons">add</i>
        </a>
      </div>

  </div>
</div>

   </body>
 </html>
