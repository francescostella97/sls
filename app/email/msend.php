
<?php
$type = $_POST['type'];
if($type == 3){
  $destination = $_POST['single_email'];
}
if($type == 2){
  $list = array();
  $list =  $_POST['list'];
}
if($type == 1){
  $group = array();
  $group = $_POST['group'];
}

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require '../commons/php/PHPMailAutoload.php';
require '../commons/php/PHPMailer.php';
require '../commons/php/SMTP.php';
require '../commons/php/Exception.php';

$mail = new PHPMailer;              //Create a new PHPMailer instance
$mail->isSMTP();                    //Tell PHPMailer to use SMTP
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
$mail->Host = 'smtp.office365.com'; //Set the hostname of the mail server
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

$mail->Port = 587;                  //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->SMTPSecure = 'tls';          //Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPAuth = true;             //Whether to use SMTP authentication
$mail->Username = $_POST['from'];   //Username to use for SMTP authentication - use full email address for gmail
$mail->Password = $_POST['pwd'];    //Password to use for SMTP authentication
$mail->setFrom($_POST['from']);     //Set who the message is to be sent from
//Set an alternative reply-to address
//$mail->addReplyTo('replyto@example.com', 'First Last');
$mail->Subject = $_POST['object'];      //Set the subject line
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$signature = file_get_contents('../../home.html');
$mail->msgHTML($_POST['object'].$signature, dirname(__FILE__));
$mail->AltBody = 'This is a plain-text message body';     //Replace the plain text body with one created manually
//$mail->addAttachment('images/phpmailer_mini.png');      //Attach an image file

//sending to a single email
if($type == 3){
  $mail->addAddress($_POST['single_email']);  //Set who the message is to be sent to
  if (!$mail->send()) {       //send the message, check for errors
      echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
      echo "Message sent!";
      //Section 2: IMAP
      //Uncomment these to save your message in the 'Sent Mail' folder.
      #if (save_mail($mail)) {
      #    echo "Message saved!";
      #}
  }
}

// sending to a list of emails
if($type == 2){
  include '../commons/php/db_connection.php';
  $connection = OpenCon();

  $list = array();
  $list =  $_POST['list'];
  foreach ($list as $destination) {
    $sql = 'SELECT family.email FROM family WHERE id = '.$destination;
    $result = mysqli_query($connection, $sql);
    $row = mysqli_fetch_assoc($result);
    $mail->addAddress($row['email']);  //Set who the message is to be sent to
    if (!$mail->send()) {       //send the message, check for errors
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        #if (save_mail($mail)) {
        #    echo "Message saved!";
        #}
    }
  }
}

// sending to groups
if($type == 1){
  $groups = array();
  $groups = $_POST['group'];
  foreach ($groups as $group) {
    $sql = 'SELECT family.id, family.email FROM family, hfgroup, f_g WHERE family.id = f_g.id_family AND hfgroup.id = f_g.id_group AND hfgroup.id = '.$group;
    include '../commons/php/db_connection.php';
    $connection = OpenCon();
    $result = mysqli_query($connection, $sql);
    while($row = mysqli_fetch_assoc($result)) {
      $destination = $row['email'];

      $mail->addAddress($destination);  //Set who the message is to be sent to
      if (!$mail->send()) {       //send the message, check for errors
          echo "Mailer Error: " . $mail->ErrorInfo;
      } else {
          echo "Message sent!";
          //Section 2: IMAP
          //Uncomment these to save your message in the 'Sent Mail' folder.
          #if (save_mail($mail)) {
          #    echo "Message saved!";
          #}
      }
    }
  }
}

// $mail->addAddress('francesco.stella97@gmail.com');  //Set who the message is to be sent to
// if (!$mail->send()) {       //send the message, check for errors
//     echo "Mailer Error: " . $mail->ErrorInfo;
// } else {
//     echo "Message sent!";
//     //Section 2: IMAP
//     //Uncomment these to save your message in the 'Sent Mail' folder.
//     #if (save_mail($mail)) {
//     #    echo "Message saved!";
//     #}
// }

//Section 2: IMAP
//IMAP commands requires the PHP IMAP Extension, found at: https://php.net/manual/en/imap.setup.php
//Function to call which uses the PHP imap_*() functions to save messages: https://php.net/manual/en/book.imap.php
//You can use imap_getmailboxes($imapStream, '/imap/ssl') to get a list of available folders or labels, this can
//be useful if you are trying to get this working on a non-Gmail IMAP server.

// function save_mail($mail)
// {
//     //You can change 'Sent Mail' to any other folder or tag
//     $path = "{outlook.office365.com:993/imap/ssl}[Gmail]/Sent Mail";
//
//     //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
//     $imapStream = imap_open($path, $mail->Username, $mail->Password);
//
//     $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
//     imap_close($imapStream);
//
//     return $result;
// }
?>
