<!DOCTYPE html>
 <html>
   <head>
     <style >
       a:hover {
        cursor: pointer;
      }
            .editableBox {
              z-index: -1;	position: absolute;


      }

      .timeTextBox {

          position: absolute;

      }
     </style>


     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
     <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>
     <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="../commons/js/jquery.easy-autocomplete.min.js"></script>
  <link rel="stylesheet" href="../commons/css/easy-autocomplete.min.css">
     <!--Let browser know website is optimized for mobile-->
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>



   </head>

   <body>
     <!--Import jQuery before materialize.js-->
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
     <script>


        function insert(text){
          $('#textarea1').val($('#textarea1').val() + " " + text + " ")
          $('#targetlabel').addClass('active');
        }
          $(document).ready(function() {
            $(".editableBox").change(function(){
                    $(".timeTextBox").val($(".editableBox option:selected").html());
                });

          $('.tooltipped').tooltip({delay: 50});
          $('select').material_select();
          $('input#input_text, textarea#object').characterCounter();

          $('input#input_text, textarea#textarea1').characterCounter();
       });

       function email_type(){
         var selected = $('input[name=type]:checked').val();
         if(selected == 1) {
           document.getElementById('single').disabled = true;
           document.getElementById('list').disabled = true;
           document.getElementById('group').disabled = false;
          }
          if(selected == 2) {
            document.getElementById('single').disabled = true;
            document.getElementById('group').disabled = true;
            document.getElementById('list').disabled = false;

           }
           if(selected == 3) {
             document.getElementById('group').disabled = true;
             document.getElementById('list').disabled = true;
             document.getElementById('single').disabled = false;
            }
            $('#list').material_select();
            $('#group').material_select();
       }
     </script>


      <?php include '../commons/php/header.php'; ?>
  <div class="row">

    <form method="post" action="msend.php" class="col s12" style="padding-top:25px;">
      <div class="row col s12">
      <div class="input-field col s3">
            <input name="from"  value=""  type="text" class="validate">
            <label class="active" for="first_name2">Username</label>
      </div>
      <div class="input-field col s3">
            <input type="password" name="pwd" value=""  type="text" class="validate">
            <label class="active" for="first_name2">Password</label>
      </div>
    </div>

      <div class="row col s12">
          <label >Choose a email type</label>
        <p>
          <input onchange="email_type()" value="1 " name="type" type="radio" id="test1" />
          <label for="test1">Group Email</label>

          <input onchange="email_type()" value="2" name="type" type="radio" id="test2" />
          <label for="test2">Multiple Email</label>

          <input onchange="email_type()" value="3" name="type" type="radio" id="test3" />
          <label for="test3">Single Email</label>
        </p>
      </div>


          <?php
            include '../commons/php/db_connection.php';
            $connection = OpenCon();

            $sql = 'SELECT id,email FROM `family` WHERE email != "" ORDER BY `family`.`email` ASC';
            $result = mysqli_query($connection, $sql);
            $array = array();
            print '<div class="row col s6"> <div class="input-field col s12">
              <select name="list[]" multiple id="list">
                <option value="" disabled selected>Choose your option</option>';
            while($row = mysqli_fetch_assoc($result)) {
                //$array[] = array('key' => $row['id'],'name' => $row['email']);
                print '<option value="'.$row['id'].'">'.$row['email'].'</option>';
            }
            print '</select>
            <label>Choose Families</label>


            </div>  <div class="input-field col s12">
            <input name="single_email" id="single" type="email" class="validate">
            <label for="email" data-error="wrong" data-success="right">Email</label>
            </div></div>';
            $sql = 'SELECT `hfgroup`.`id`,COUNT(`family`.`id`) AS "no", `hfgroup`.`name` FROM `hfgroup`,`family`,`f_g` WHERE `f_g`.`id_family` = `family`.`id` AND `f_g`.`id_group` = `hfgroup`.`id` GROUP BY `hfgroup`.`id` ORDER BY `hfgroup`.`name` ASC';
            $result = mysqli_query($connection, $sql);
            $array = array();
            print '<div class="row col s6"> <div class="input-field col s12">
              <select name="group[]" multiple id="group">
                <option value="" disabled selected>Choose your option</option>';
            while($row = mysqli_fetch_assoc($result)) {
                //$array[] = array('key' => $row['id'],'name' => $row['email']);
                print '<option value="'.$row['id'].'">'.$row['name'].' ( '.$row['no'].' partecipants)</option>';
            }
            print '</select>

              <label>Choose group</label>
            </div></div>';


          ?>


          <div class="row col s12">
            <div class="input-field col s12">
              <textarea  name="object" id="object" class="materialize-textarea" data-length="50"></textarea>
              <label for="object">Object</label>
            </div>
          </div>

      <div class="row col s12">
        <div class="input-field col s12">
          <textarea style="height: 100px;
     " name="body" id="textarea1" class="materialize-textarea" rows="10" data-length="300"></textarea>
          <label for="textarea1" id="targetlabel">Body</label>
        </div>
      </div>
      <div class="row col s12">      <div style="height:60px;">
            </div>
            </div>
      <div class="row" style="
          position: fixed;
          left: 23px;
          bottom: 23px;
          padding-top: 15px;
          margin-bottom: 0;
          z-index: 997
      ">


        <div onclick="insert('Dear Student')" class="chip"><a>Dear Student</a></div>
        <div onclick="insert('Dear Family')" class="chip"><a>Dear Family</a></div>

      </div>

      <div class="fixed-action-btn">
        <button class="btn waves-effect waves-light" type="submit" name="action">Send
         <i class="material-icons right">send</i>
       </button>

      </div>




    </form>
  </div>

   </body>
 </html>
