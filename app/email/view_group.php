<!DOCTYPE html>
 <html>
   <head>

     <!--Import Google Icon Font-->
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
     <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
     <script src="../commons/js/jquery.toast.js"></script>
     <script src="../commons/js/jquery.toast.min.js"></script>
     <link href="../commons/css/jquery.toast.css" rel="stylesheet" type="text/css">
     <link href="../commons/css/jquery.toast.min.css" rel="stylesheet" type="text/css">
     <!--Let browser know website is optimized for mobile-->
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   </head>

   <body>

     <script>

     var updates = true;
     var emailList = []
     function updateGroup(id_group){
      //  console.log(id_group);
      //  console.log(document.getElementById('info').elements["group_name"].value);
      //  console.log(emailList);
       if(updates){
          if (confirm('Are you sure you want to update this element?')) {

           $.ajax({
            type: "POST",
            url: "src/group/update.php",
            data: {id : id_group, group_name : document.getElementById('info').elements["group_name"].value, emails : emailList},
            success: function(){ // trigger when request was successfull
              $.toast({
                 text: "Group updated", // Text that is to be shown in the toast
                 heading: 'Success', // Optional heading to be shown on the toast
                 icon: 'success', // Type of toast icon
                 showHideTransition: 'slide', // fade, slide or plain
                 allowToastClose: true, // Boolean value true or false
                 hideAfter: 2000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                 stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                 position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                 textAlign: 'left',  // Text alignment i.e. left, right or center
                 loader: false,  // Whether to show loader or not. True by default
                 loaderBg: '#9EC600',  // Background color of the toast loader
             });
             setTimeout(function(){
            window.location.href = 'groups.php'
            }, 2000);
          }
          });
        }
      } else{
          window.location.href = 'groups.php'
      }
     }
     function deleteGroup(id){
         if (confirm('Are you sure you want to delete this element?')) {
                     $.ajax({
                      type: "POST",
                      url: "group/delete.php",
                      data: 'id=' + id,
                      success: function(){ // trigger when request was successfull
                        $.toast({
                           text: "Group deleted", // Text that is to be shown in the toast
                           heading: 'Success', // Optional heading to be shown on the toast
                           icon: 'success', // Type of toast icon
                           showHideTransition: 'slide', // fade, slide or plain
                           allowToastClose: true, // Boolean value true or false
                           hideAfter: 2000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                           stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                           position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                           textAlign: 'left',  // Text alignment i.e. left, right or center
                           loader: false,  // Whether to show loader or not. True by default
                           loaderBg: '#9EC600',  // Background color of the toast loader
                       });
                       setTimeout(function(){
                      window.location.href = 'groups.php'
                      }, 2000);
                    }
                    });
        }
     }
     function changeUpdateStatus(){
       if(updates == false){
           updates = !updates;
           document.getElementById("group_name").disabled = false;
           $.toast({
              text: "Updates Enabled", // Text that is to be shown in the toast
              heading: 'Success', // Optional heading to be shown on the toast
              icon: 'info', // Type of toast icon
              showHideTransition: 'slide', // fade, slide or plain
              allowToastClose: true, // Boolean value true or false
              hideAfter: 2000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
              stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
              position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
              textAlign: 'left',  // Text alignment i.e. left, right or center
              loader: false,  // Whether to show loader or not. True by default
              loaderBg: '#9EC600',  // Background color of the toast loader
          });
        }
        else{
          updates = !updates;
          document.getElementById("group_name").disabled = true;
          $.toast({
             text: "Updates Disabled", // Text that is to be shown in the toast
             heading: 'Success', // Optional heading to be shown on the toast
             icon: 'info', // Type of toast icon
             showHideTransition: 'slide', // fade, slide or plain
             allowToastClose: true, // Boolean value true or false
             hideAfter: 2000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
             stack: 5, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
             position: 'bottom-left', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
             textAlign: 'left',  // Text alignment i.e. left, right or center
             loader: false,  // Whether to show loader or not. True by default
             loaderBg: '#9EC600',  // Background color of the toast loader
         });
        }
     }

      function removeEmail(id,email){
        console.log(id);
          if(updates){
            var i = emailList.indexOf(id);
            if(i != -1) {
            	emailList.splice(i, 1);
              var formInfo = document.getElementById('info').elements["emails"];
              formInfo.value = emailList;
            }
console.log(emailList);
          }


      }
      function addEmail(id,email){
        if(updates){

          if (emailList.indexOf(id) == -1) {
            var chip = document.createElement('div')
            chip.className = "chip"
            chip.innerHTML = email + '<i onclick="removeEmail(&quot;' + id + '&quot;,&quot;' + email + '&quot;)" class="close material-icons">close</i>'
            document.getElementById('chipContainer').appendChild(chip)
            emailList.push(id)
            var formInfo = document.getElementById('info').elements["emails"];
            formInfo.value = emailList;
          }



        }
      }


      $(document).ready(function() {
        $('.tooltipped').tooltip({delay: 50});
        $('select').material_select();
        $('input#input_text, textarea#textarea1').characterCounter();
      });
      $(function() {
        var options = {
          url: "emails.json",

          getValue: "label",

          list: {
            match: {
              enabled: true
            }
          }
        };
        console.log(options);
        $("autocomplete").autocomplete(options);
         $('.chips').material_chip();
        });

     </script>


     <?php include '../commons/php/header.php'; ?>
     <?php
       $id = $_GET['id'];
       include '../commons/php/db_connection.php';
       $connection = OpenCon();
       $sql = 'SELECT * FROM `hfgroup` WHERE id='.$id;
       $result = mysqli_query($connection, $sql);
       $array = array();
       $row = mysqli_fetch_assoc($result)

     ?>

  <div class="row">

    <form id="info" action="src\group\add.php" method="post" class="col s12" style="padding-top:25px;">
      <input type="hidden" name="emails" value="" />
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">group</i>
          <input id="group_name" type="text" class="validate" name="group_name" value="<?php echo $row['name']?>" disabled>
          <label for="icon_prefix">Group Name</label>
        </div>

      </div>
      <div class="row input-field col s6">

        <ul class="collection with-header nav">
         <li class="collection-header"><h4>Choose Group's Components</h4></li>

         <?php
           $id = $_GET['id'];

           $sql = 'SELECT id,email FROM `family` WHERE email != "" ORDER BY `family`.`email` ASC';
           $result = mysqli_query($connection, $sql);
           $array = array();
           print '<script>updates = true</script>';
           while($row = mysqli_fetch_assoc($result)) {
               //$array[] = array('key' => $row['id'],'name' => $row['email']);
               print '<li class="collection-item"><div>'.$row['email'].'<a onclick="addEmail(&quot;'.$row['id'].'&quot;,&quot;'.$row['email'].'&quot;)" href="#!" class="secondary-content"><i class="material-icons">send</i></a></div></li>';
               //print '<script>addEmail("'.$row['id'].'")</script>';
           }


         ?>
       </ul>

      </div>
      <div class="row input-field col s6" id="chipContainer">

      </div>
      <?php
        $id = $_GET['id'];

        $sql = 'SELECT  `family`.`id`,`family`.`email` FROM `f_g`,`family` WHERE `family`.`id` = `f_g`.`id_family` AND `f_g`.`id_group` = '.$id;
        $result = mysqli_query($connection, $sql);
        $array = array();

        while($row = mysqli_fetch_assoc($result)) {
            //$array[] = array('key' => $row['id'],'name' => $row['email']);
            //print '<li class="collection-item"><div>'.$row['email'].'<a  onclick="addEmail(&quot;'.$row['id'].'&quot;,&quot;'.$row['email'].'&quot;)" href="#!" class="secondary-content"><i class="material-icons">send</i></a></div></li>';
            //print '<script>addEmail("'.$row['id'].'")</script>';
            print '<script>addEmail("'.$row['id'].'","'.$row['email'].'")</script>';
        }
        print '<script>updates = false;</script>';

      ?>




      <div class="fixed-action-btn">
          <a onclick="updateGroup(<?php echo $id;?>)" class="btn-floating btn-large yellow">
            <i class="large material-icons">check</i>
          </a>
          <ul>
            <li><a data-position="left" data-delay="50" data-tooltip="Send email to this group" onclick="" class="tooltipped btn-floating green"><i class="material-icons">email</i></a></li>
            <li><a data-position="left" data-delay="50" data-tooltip="Delete group" onclick="deleteGroup(<?php echo $id;?>)" class="tooltipped btn-floating red"><i class="material-icons">delete</i></a></li>
            <li><a data-position="left" data-delay="50" data-tooltip="Update group" onclick="changeUpdateStatus()" class="tooltipped btn-floating blue"><i class="material-icons">edit</i></a></li>


          </ul>
      </div>





    </form>
  </div>

   </body>
 </html>
