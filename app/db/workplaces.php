<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT * FROM `workplace`, `address` WHERE `workplace`.`id_area` = `address`.`id`';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('key' => $row['id'],'name' => $row['name'],
    'hours' => $row['working_hours'],
    'dresscode' => $row['dresscode'], 'manager' => $row['manager'],
    'add1' => $row['add1'],'add2' => $row['add2'],
    'area' => $row['area'],'county' => $row['county'],
    'phone' => $row['phone']);
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
