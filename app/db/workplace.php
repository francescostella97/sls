
<html>
<head>
    <title>AJAX PHP Search Engine Script for MySQL Database</title>

</head>
<body>
      <?php include '../commons/php/header.php'; ?>
  <div class="container-fluid">

    <div class="row" >
      <div id="master" class="col-12" style="position:absolute;z-index:0">
      </div>

      <div id="form" class="col-2" style="position:absolute;z-index:1">
      </div>
    </div>

  </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link href="http://code.fancygrid.com/fancy.min.css" rel="stylesheet">
    <script src="http://code.fancygrid.com/fancy.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../commons/js/materialize.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
    <script src="../commons/js/typeahead.jquery.js"></script>
    <script src="../commons/js/jquery.toast.js"></script>
    <script src="../commons/js/jquery.toast.min.js"></script>
    <link href="../commons/css/jquery.toast.css" rel="stylesheet" type="text/css">
    <link href="../commons/css/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!--<link href="css/fancy.min.css" rel="stylesheet">
    <script src="js/fancy.min.js"></script>-->

<div id="container"></div>

    <script>
    $(document).ready(function(){
   $('.tooltipped').tooltip({delay: 50});
 });
    Fancy.enableCompo();


    function phoneInputFn(value) {
  value = parseInt(value.replace(/\-/g, ''));

  if (isNaN(value)) {
    value = '';
  } else {
    value = String(value);
  }

  switch (value.length) {
    case 0:
    case 1:
    case 2:
      break;
    case 4:
    case 5:
    case 6:
      value = value.replace(/^(\d{3})/, "$1-");
      break;
    case 7:
    case 8:
    case 9:
      value = value.replace(/^(\d{3})(\d{3})/, "$1-$2-");
      break;
    case 10:
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      break;
    default:
      value = value.substr(0, 10);
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
  }

  return value;
}

    function upInputFn(value) {
      value = value.toString().toLocaleUpperCase();

      return value;
    }
    var selectedObj
    var selectedChild
    String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
    $(function() {
      var obj



var main_grid = new FancyGrid({
  title: 'Workplaces Database',
  renderTo: 'master',
  height: 600,
  theme: 'blue',
  selModel: 'rows',
  data: {
    proxy: {
      url: 'workplaces.php'
    }
  },
  defaults: {
    type: 'string',
    width: 120,
    resizable: true,
    sortable: true,
    editable: true,
    filter: {
        header: true,
        emptyText: 'Search'
      }
  },
  paging: {
	pageSize: 20,
    pageSizeData: [5,10,20,50,100]
  },
  clicksToEdit: 2,
    rowEdit: true,
    tbar: [{
      type: 'button',
      text: 'Refresh',
      handler:function(){
        location.reload();
      }
    },{
  type: 'button',
  text: 'Add',
  handler: function(){
    //window.location.href = "http://stackoverflow.com";
    $(function() {
      var obj

          var form = new FancyForm({
          renderTo: 'form',
          title: {
          text: 'Register',
          tools : [{
                    text: 'Close',
                    handler: function() {
                      this.hide();
                    }
                  }]
          },
          draggable: true,
          width: 350,
          height: 400,
          tabs: ['General','Location'],
              items: [  {type: 'tab',
                          items: [{
                            type: 'string',
                            label: 'Name',
                            emptyText: 'Name',
                            name: 'workplace_name'
                          },{
                            type: 'string',
                            label: 'Working Hours',
                            emptyText: '10:00 - 17:00',
                            name: 'workplace_hours'
                          },{
                            type: 'string',
                            label: 'Dresscode',
                            emptyText: 'Dresscode',
                            name: 'workplace_dresscode'
                          },
                          {
                            type: 'string',
                            label: 'Manager',
                            emptyText: 'Manager',
                            name: 'workplace_manager'
                          },
                          {
                           label: 'Phone',
                           name: 'workplace_phone',
                           emptyText: 'xxx-xxx-xxxx',
                           format: {
                             inputFn: phoneInputFn
                           }
                         }]
                      },

                      {
                      type: 'tab',
                      label: 'Location Information',
                      items: [{
                        type: 'string',
                        label: 'Address 1',
                        emptyText: 'Address 1',
                        name: 'workplace_add1'
                      }, {
                        type: 'string',
                        label: 'Address 2',
                        emptyText: 'Address 2',
                        name: 'workplace_add2'
                      }, {
                        type: 'combo',
                        label: 'Area',
                        emptyText: '',
                        name: 'workplace_area',
                        data: {
                          proxy: {
                            url: 'areas.php'
                          }
                        },
                          displayKey: 'area',
                          valueKey: 'key',
                      },{
                        type: 'combo',
                        label: 'County',
                        emptyText: '',
                        name: 'workplace_county',
                        data: {
                          proxy: {
                            url: 'counties.php'
                          }
                        },displayKey: 'name',
                        valueKey: 'key',
                      }]
                    }

              ],
          buttons: ['side', {
          text: 'Clear',
          handler: function() {
            this.clear();
          }
          }, {
              text: 'Register',
              handler: function() {
                console.log(JSON.stringify(this.get()));
                $.ajax({
                   url: 'src/workplace/insert.php',
                   type: 'POST',
                   data: {
                     workplace:JSON.stringify(this.get())
                   },
                   success: function(data) {
                       console.log(data); // Inspect this in your console
                       if(data.data == "success") {
                         $.toast('Row Inserted')
                         form.clear();
                         form.hide();
                         main_grid.load();
                       }
                   }, error: function (request, error) {
                        alert("AJAX Call Error: " + error);
                    }
               });
              }
          }]
          });


    });


  }
},{
    type: 'button',
    text: 'Delete',
    action: 'remove'
  }],
  columns: [
  {
    text: 'Workplace',
    columns: [{
      index: 'key',
      title: 'Id',
      editable: false,
      width: 70,
    },{
      type:'string',
    index: 'name',
    title: 'Name',
  },{
    index: 'hours',
    title: 'Working Hours',

  },{
    index: 'dresscode',
    title: 'Dresscode'
  },{
    index: 'manager',
    title: 'Manager'
  },{
    index: 'add1',
    title: 'Add1'
  },{
    index: 'add2',
    title: 'Add2'
  },{
    index: 'area',
    title: 'Area',
    type: 'combo',
    data: {proxy: {
      url: 'areas.php'
    }},
    displayKey: 'area',
      valueKey: 'key',
  },{
    index: 'county',
    title: 'County',
    type: 'combo',
    data: {proxy: {
      url: 'counties.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'phone',
    title: 'Phone'
  }]}
  ],
  events:[{
    update: function(grid){
      console.log("updating row " + JSON.stringify(selectedObj));
      $.ajax({
         url: 'src/workplace/update.php',
         type: 'POST',
         data: {
           workplace:JSON.stringify(selectedObj)
         },
         success: function(data) {
             console.log(data.data); // Inspect this in your console
             if(data.data == "success") $.toast('Row Updated')
         }
     });
      //console.log(JSON.stringify(grid.store.data));
    }
  },{
    remove: function(grid,id,item){
      console.log(item.data.key);
      $.ajax({
         url: 'src/workplace/delete.php',
         type: 'POST',
         data: {id:item.data.key},
         success: function(data) {
             console.log(data); // Inspect this in your console
              if(data.data == "success") $.toast('Row Deleted')
         }
     });
    }
  },{
    rowclick: function(grid, o){
      selectedObj = o.data;
    }
  }]

});

});

    </script>
</body>
</html>
