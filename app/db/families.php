<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT family.id,name,wife_name, address.area FROM `family`,address WHERE address.id = family.id_address ORDER BY `family`.`name`';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('key' => $row['id'],'name' => $row['name']." ".$row['wife_name']." (".$row['area'].")");
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
