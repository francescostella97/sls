<html>
<head>
    <title>AJAX PHP Search Engine Script for MySQL Database</title>

</head>
<body>
  <?php include '../commons/php/header.php'; ?>
  <div class="container-fluid">

    <div class="row">
      <div id="master" class="col-8" style="z-index:0">
      </div>
      <div id="children_grid" class="col-3" >
      </div>
      <div id="form" class="col-1" style="position: absolute;
    top: 8px;
    left: 16px;
    font-size: 18px;">
      </div>
    </div>

  </div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link href="http://code.fancygrid.com/fancy.min.css" rel="stylesheet">
<script src="http://code.fancygrid.com/fancy.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../commons/js/materialize.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../commons/js/materialize.min.js"></script>
<script src="../commons/js/typeahead.jquery.js"></script>
<script src="../commons/js/jquery.toast.js"></script>
<script src="../commons/js/jquery.toast.min.js"></script>
<link href="../commons/css/jquery.toast.css" rel="stylesheet" type="text/css">
<link href="../commons/css/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!--<link href="css/fancy.min.css" rel="stylesheet">
    <script src="js/fancy.min.js"></script>-->

<div id="container"></div>

    <script>
    $(document).ready(function(){
   $('.tooltipped').tooltip({delay: 50});
 });
    Fancy.enableCompo();


    function phoneInputFn(value) {
  value = parseInt(value.replace(/\-/g, ''));

  if (isNaN(value)) {
    value = '';
  } else {
    value = String(value);
  }

  switch (value.length) {
    case 0:
    case 1:
    case 2:
      break;
    case 4:
    case 5:
    case 6:
      value = value.replace(/^(\d{3})/, "$1-");
      break;
    case 7:
    case 8:
    case 9:
      value = value.replace(/^(\d{3})(\d{3})/, "$1-$2-");
      break;
    case 10:
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      break;
    default:
      value = value.substr(0, 10);
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
  }

  return value;
}
    function displayChildrenGrid(){

    }
    function upInputFn(value) {
      value = value.toString().toLocaleUpperCase();

      return value;
    }
    var selectedObj
    var selectedChild
    String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
    $(function() {
      var obj
              var children_grid = new FancyGrid({
                title: 'Children',
                renderTo: 'children_grid',
                height: 600,
                width:400,

                selModel: 'rows',
                clicksToEdit: 2,
                  rowEdit: true,
                theme: 'blue',
                data: {
                    fields: ['name','sex','dob','age']
                  },
                defaults: {
                  type: 'string',
                  width: 100,
                  resizable: true,
                  editable: true,
                  sortable: true,
                  filter: {
                      header: true,
                      emptyText: 'Search'
                    }
                },
                paging: {
              	pageSize: 20,
                  pageSizeData: [5,10,20,50,100]
                },
                tbar: [{
                  type: 'button',
                  text: 'Refresh',
                  handler:function(){
                    location.reload();
                  }},{
                type: 'button',
                text: 'Add',
                handler: function(){
                  console.log(selectedObj);
                  //window.location.href = "http://stackoverflow.com";
                  $(function() {
                    var obj

                        var form = new FancyForm({
                        renderTo: 'form',
                        title: {
                        text: 'Register',
                        tools : [{
                                  text: 'Close',
                                  handler: function() {
                                    this.hide();
                                  }
                                }]
                        },
                        draggable: true,
                        width: 300,
                        height: 200,
                        items: [{
                          type: 'combo',
                          label: 'Name',
                          emptyText: 'Name',
                          name: 'name',
                          data: {
                            proxy: {
                              url: 'names.php'
                            }
                          },
                            displayKey: 'name',
                            valueKey: 'key',
                        },{
                          type: 'combo',
                          label: 'Sex',
                          emptyText: 'Sex',
                          name: 'sex',
                          data: [
                             {key: 0, sex: 'M'},
                             {key: 1, sex: 'F'},
                           ],
                           displayKey: 'sex',
                           valueKey: 'sex',
                        },{
                          type: 'date',
                          label: 'Dob',
                          emptyText: '',
                          name: 'dob',
                          format: {
                            read: 'd/m/Y',
                            write: 'd/m/Y',
                            edit: 'd/m/Y',
                          },
                        }],

                        buttons: ['side', {
                        text: 'Clear',
                        handler: function() {
                          this.clear();
                        }
                        }, {
                            text: 'Register',
                            handler: function() {
                              //
                              var string = JSON.stringify(this.get());
                              var string = string.splice(1,0,'"id":'+'"'+selectedObj.FamId + '",' );
                              console.log(string);
                              //console.log(JSON.stringify(this.get()));
                              $.ajax({
                                 url: 'src/children/add.php',
                                 type: 'POST',
                                 data: {
                                   //id:selectedObj.FamId,
                                   child:string
                                 },
                                 success: function(data) {
                                     console.log(data); // Inspect this in your console
                                     if(data.data == "success") {
                                       $.toast('Row Inserted')
                                       form.clear();
                                       form.hide();
                                       children_grid.load();

                                     }
                                 }
                             });
                             //console.log("im here now");
                            }
                        }]
                        });


                  });

                }
              },{
                  type: 'button',
                  text: 'Delete',
                  action: 'remove'

                }],

                columns: [{
                    index: 'name',
                    title: 'Name',
                    type: 'combo',
                    data: {proxy: {
                      url: 'names.php'
                    }},
                    displayKey: 'name',
                      valueKey: 'key',
                  },{
                    index: 'sex',
                    title: 'Sex'
                  }, {
                    index: 'dob',
                    title: 'dob'
                  },{
                    type:'number',
                    index: 'age',
                    title: 'Age',
                    spin: true,
                    width: 60,
                    editable: false
                  }],
                    events:[{
                      update: function(grid){
                        //console.log("modificato");
                        //sconsole.log(selectedChild);
                        $.ajax({
                        url: 'src/children/update.php',
                           type: 'POST',
                           dataType: 'text',
                           data: {
                             child:JSON.stringify(selectedChild)
                           },
                           success: function(data) {
                               console.log(data.data); // Inspect this in your console
                               if(data.data == "success") {
                                 $.toast('Row Updated')
                               }
                           }, error: function (request, error) {
                                alert("AJAX Call Error: " + error);
                            }
                       });
                        //console.log(JSON.stringify(grid.store.data));
                      }
                    },{
                      remove: function(grid,id,item){
                        //console.log("removing " + item.data.id);
                        var request = $.ajax({
                           url: 'src/children/delete.php',
                           type: 'POST',
                           data: {
                             id:item.data.id
                           },
                           success: function(data) {
                               console.log(data); // Inspect this in your console
                                if(data.data == "success") {
                                  $.toast('Row Deleted')
                                }
                           },error: function (request, error) {
                                alert("AJAX Call Error: " + error);
                            }
                       });

                      }
                    },{
                      rowclick: function(grid, o){
                        selectedChild = o.data;
                        //console.log(selectedChild);
                      }
                    }





                  ]
              });


var main_grid = new FancyGrid({
  title: 'HFMasterDatabase',
  renderTo: 'master',
  height: 600,
  theme: 'blue',
  selModel: 'rows',
  data: {
    proxy: {
      url: 'data.php'
    }
  },
  defaults: {
    type: 'string',
    width: 100,
    resizable: true,
    sortable: true,
    editable: true,
    filter: {
        header: true,
        emptyText: 'Search'
      }
  },
  paging: {
	pageSize: 20,
    pageSizeData: [5,10,20,50,100]
  },
  clicksToEdit: 2,
    rowEdit: true,
    tbar: [{
      type: 'button',
      text: 'Refresh',
      handler:function(){
        location.reload();
      }
    },{
  type: 'button',
  text: 'Add',
  handler: function(){
    //window.location.href = "http://stackoverflow.com";
    $(function() {
      var obj

          var form = new FancyForm({
          renderTo: 'form',
          title: {
          text: 'Register',
          tools : [{
                    text: 'Close',
                    handler: function() {
                      this.hide();
                    }
                  }]
          },
          draggable: true,
          width: 350,
          height: 500,
          tabs: ['General','Location', 'Parents','Children', 'Pets'],
              items: [  {type: 'tab',
                          items: [{
                            type: 'string',
                            label: 'Family Name',
                            emptyText: 'Family Name',
                            name: 'Family Name'
                          },{
                            type: 'string',
                            label: 'Phone',
                            emptyText: 'x-xxx-xxx',
                            format: {
                              inputFn: phoneInputFn
                            },
                            name: 'Phone'
                          },{
                            type: 'string',
                            label: 'Email',
                            emptyText: 'Email',
                            name: 'Email'
                          },{
                            type: 'string',
                            label: 'SMoker (Y/N)',
                            emptyText: 'Smoker',
                            name: 'Smoker'
                          },{
                            type: 'date',
                            label: 'Availability',
                            emptyText: '',
                            name: 'Availability'
                          },{
                            type: 'date',
                            label: 'Last Update',
                            emptyText: '',
                            name: 'LastUpdate'
                          },{
                            type: 'date',
                            label: 'Last Visit',
                            emptyText: '',
                            name: 'LastVisit'
                          },{
                            type: 'textarea',
                            label: 'Comment',
                            emptyText: 'Comment',
                            name: 'Comment'
                          }]
                      },

                        {
                        type: 'tab',
                        label: 'Location Information',
                        items: [{
                          type: 'string',
                          label: 'Number',
                          emptyText: 'Number',
                          name: 'No'
                        }, {
                          type: 'string',
                          label: 'Street',
                          emptyText: 'Street',
                          name: 'Street'
                        }, {
                          type: 'string',
                          label: 'Address 2',
                          emptyText: '',
                          name: 'Add2'
                        }, {
                          type: 'combo',
                          label: 'Area',
                          emptyText: '',
                          name: 'Area',
                          data: {
                            proxy: {
                              url: 'areas.php'
                            }
                          },
                            displayKey: 'area',
                            valueKey: 'key',
                        },{
                          type: 'combo',
                          label: 'County',
                          emptyText: '',
                          name: 'County',
                          data: {
                            proxy: {
                              url: 'counties.php'
                            }
                          },displayKey: 'name',
                          valueKey: 'key',
                        }, {
                          type: 'string',
                          label: 'Eircode',
                          name: 'Eircode',
                          format:{
                            inputFn : upInputFn
                          }
                        }]
                      }
                      ,
                      {
                      type: 'tab',
                      label: 'Wife Information',
                      items: [{
                        type: 'combo',
                        label: 'Name',
                        emptyText: 'Name',
                        name: 'WifeName',
                        data: {
                          proxy: {
                            url: 'names.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'name',
                      }, {
                        label: 'Mobile',
                        name: 'WifeMobile',
                        emptyText: 'xxx-xxx-xxxx',
                        format: {
                          inputFn: phoneInputFn
                        }
                      }, {
                        type: 'combo',
                        label: 'Occupation',
                        emptyText: '',
                        name: 'WifeOccupation',
                        data: {
                          proxy: {
                            url: 'occupations.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'name',
                      }, {
                        type: 'combo',
                        label: 'Name',
                        emptyText: 'Name',
                        name: 'HusbandName',
                        data: {
                          proxy: {
                            url: 'names.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'name',
                      }, {
                        label: 'Mobile',
                        name: 'HusbandMobile',
                        emptyText: 'xxx-xxx-xxxx',
                        format: {
                          inputFn: phoneInputFn
                        }
                      }, {
                        type: 'combo',
                        label: 'Occupation',
                        emptyText: '',
                        name: 'HusbandOccupation',
                        data: {
                          proxy: {
                            url: 'occupations.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'name',
                      }]//
                    },{
                      type: 'tab',
                      label: 'Children',
                      items: [{
                        type: 'string',
                        label: 'Number',
                        emptyText: '',
                        name: 'NoChildren'
                      },{
                        type: 'textarea',
                        label: 'Comment',
                        emptyText: 'Comment',
                        name: 'ChildrenComment'
                      }
                      ]
                    },{
                      type: 'tab',
                      label: 'Pets',
                      items: [{
                        type: 'string',
                        label: 'Details',
                        emptyText: 'Details',
                        name: 'Pets'
                      },{
                        type: 'string',
                        label: 'Garda Vetted (Y/N)',
                        emptyText: 'Garda Vetted',
                        name: 'Garda Vetted'
                      },
                      {
                        type: 'date',
                        label: 'Vetting Date',
                        emptyText: '',
                        name: 'Vetting Date'
                      }
                      ]
                    }

              ],
          buttons: ['side', {
          text: 'Clear',
          handler: function() {
            this.clear();
          }
          }, {
              text: 'Register',
              handler: function() {
                console.log(JSON.stringify(this.get()));
                $.ajax({
                   url: 'insert.php',
                   type: 'POST',
                   data: {
                     family:JSON.stringify(this.get())
                   },
                   success: function(data) {
                       console.log(data); // Inspect this in your console
                       if(data.data == "success") {
                         $.toast('Row Inserted')
                         form.clear();
                         form.hide();
                         main_grid.load();
                       }
                   }
               });
              }
          }]
          });


    });

  }
},{
    type: 'button',
    text: 'Delete',
    action: 'remove'
  }],
  columns: [{
    index: 'FamId',
	  width: 60,
    locked: true,
    editable: false,
    title: 'Fam Id'
  },{
    locked: true,
    index: 'Family Name',
    title: 'Family Name',
    type: 'combo',
    data: {proxy: {
      url: 'firsts.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'WifeName',
    title: 'First',
    type: 'combo',
    data: {proxy: {
      url: 'names.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  }, {
    text: 'Location',
    columns: [{
    index: 'No',
    title: 'No'
  },{
    index: 'Street',
    title: 'Street'
  },{
    index: 'Add2',
    title: 'Add2'
  },{
    index: 'Area',
    title: 'Area',
    type: 'combo',
    data: {proxy: {
      url: 'areas.php'
    }},
    displayKey: 'area',
      valueKey: 'key',
  },{
    index: 'County',
    title: 'County',
    type: 'combo',
    data: {proxy: {
      url: 'counties.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'Eircode',
    title: 'Eircode'
  }]},
  {
    index: 'Phone',
    title: 'Phone'
  },{
    index: 'Email',
    title: 'Email'
  },  {text: 'Wife Info',
      columns: [{
        index: 'WifeMobile',
        title: 'Mobile'
      },{
        index: 'WifeOccupation',
        title: 'Occupation',
        type: 'combo',
        data: {proxy: {
          url: 'occupations.php'
        }},
        displayKey: 'name',
          valueKey: 'key',
      }]}
  ,   {text: 'Husband Info',
      columns: [{
        index: 'HusbandName',
        title: 'Name',
        type: 'combo',
        data: {proxy: {
          url: 'names.php'
        }},
        displayKey: 'name',
          valueKey: 'key',
      },{
        index: 'HusbandMobile',
        title: 'Mobile'
      },{
        index: 'HusbandOccupation',
        title: 'Occupation',
        type: 'combo',
        data: {proxy: {
          url: 'occupations.php'
        }},
        displayKey: 'name',
          valueKey: 'key',
      }]},
      {text: 'Children',
      columns: [{
        index: 'NoChildren',
        title: 'No'
      },{
        index: 'Comment',
        title: 'Comment'
      }]},
  {
    index: 'Smoker',
    title: 'Smoker'
  },{
    index: 'Comments',
    title: 'Comments'
  },{
    index: 'Availability',
    title: 'Availability'
  },{
    index: 'LastUpdate',
    title: 'Last Update'
  },{
	text: 'Pets Information',
    	columns: [{
    	index: 'Pets',
    	title: 'Pets'
  	},{
   	index: 'Garda Vetted',
    	title: 'Garda Vetted'
  	},{
    	index: 'Vetting Date',
    	title: 'Vetting Date'
  	}]
  },{
    index: 'LastVisit',
    title: 'Last Visit'
  }],
  events:[{
    update: function(grid){
      console.log("updating row " + JSON.stringify(selectedObj));
      $.ajax({
         url: 'update.php',
         type: 'POST',
         data: {
           family:JSON.stringify(selectedObj)
         },
         success: function(data) {
             console.log(data.data); // Inspect this in your console
             if(data.data == "success") $.toast('Row Updated')
         }
     });
      //console.log(JSON.stringify(grid.store.data));
    }
  },{
    remove: function(grid,id,item){
      console.log(item.data.FamId);
      $.ajax({
         url: 'delete.php',
         type: 'POST',
         data: {famId:item.data.FamId},
         success: function(data) {
             console.log(data); // Inspect this in your console
              if(data.data == "success") $.toast('Row Deleted')
         }
     });
    }
  },{
    rowclick: function(grid, o){
      selectedObj = o.data;

      var options = o.value

        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(){
          if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            jsonDoc = xmlhttp.responseText;
            //alert(xmlDoc)
            obj = JSON.parse(jsonDoc)
            //console.log(obj)
          }
        }
        xmlhttp.open("GET","children.php?famId="+o.data.FamId,true)
        xmlhttp.send()

          //console.log(obj.data)
          var table_row = children_grid.get().length;
          var data_row = obj.data.length;
          //if(obj.data != "empty"){
                  if(table_row == 0){
                    //first load
                    for (var i = 0; i < obj.data.length; i++) {
                      children_grid.insert({

                        id: obj.data[i].id,
                        name: obj.data[i].name,
                        sex: obj.data[i].sex,
                        dob: obj.data[i].dob,
                        age: obj.data[i].age
                      });
                    }
                  }
                  else{
                    //other loads
                    //children_grid.clearSelection();
                    children_grid.hide();

                    children_grid = new FancyGrid({
                      title: 'Children',
                      renderTo: 'children_grid',
                      height: 500,
                      width:400,

                      selModel: 'rows',
                      clicksToEdit: 2,
                        rowEdit: true,
                      theme: 'blue',
                      data: {
                          fields: ['name','sex','dob','age']
                        },
                      defaults: {
                        type: 'string',
                        width: 100,
                        resizable: true,
                        editable: true,
                        sortable: true,
                        filter: {
                            header: true,
                            emptyText: 'Search'
                          }
                      },
                      paging: {
                      pageSize: 20,
                        pageSizeData: [5,10,20,50,100]
                      },
                      tbar: [{
                        type: 'button',
                        text: 'Refresh',
                        handler:function(){
                          location.reload();
                        }},{
                      type: 'button',
                      text: 'Add',
                      handler: function(){
                        console.log(selectedObj);
                        //window.location.href = "http://stackoverflow.com";
                        $(function() {
                          var obj

                              var form = new FancyForm({
                              renderTo: 'form',
                              title: {
                              text: 'Register',
                              tools : [{
                                        text: 'Close',
                                        handler: function() {
                                          this.hide();
                                        }
                                      }]
                              },
                              draggable: true,
                              width: 300,
                              height: 200,
                              items: [{
                                type: 'combo',
                                label: 'Name',
                                emptyText: 'Name',
                                name: 'name',
                                data: {
                                  proxy: {
                                    url: 'names.php'
                                  }
                                },
                                  displayKey: 'name',
                                  valueKey: 'key',
                              },{
                                type: 'combo',
                                label: 'Sex',
                                emptyText: 'Sex',
                                name: 'sex',

                                data: [
                                   {key: 0, sex: 'M'},
                                   {key: 1, sex: 'F'},
                                 ],
                                 displayKey: 'sex',
                                 valueKey: 'sex',
                              },{
                                type: 'date',
                                label: 'Dob',
                                emptyText: '',
                                name: 'dob',
                                format: {
                                  read: 'd/m/Y',
                                  write: 'd/m/Y',
                                  edit: 'd/m/Y',
                                }
                              }],

                              buttons: ['side', {
                              text: 'Clear',
                              handler: function() {
                                this.clear();
                              }
                              }, {
                                  text: 'Register',
                                  handler: function() {
                                    //
                                    var string = JSON.stringify(this.get());
                                    var string = string.splice(1,0,'"id":'+'"'+selectedObj.FamId + '",' );
                                    console.log(string);
                                    //console.log(JSON.stringify(this.get()));
                                    $.ajax({
                                       url: 'src/children/add.php',
                                       type: 'POST',
                                       data: {
                                         //id:selectedObj.FamId,
                                         child:string
                                       },
                                       success: function(data) {
                                           console.log(data); // Inspect this in your console
                                           if(data.data == "success") {
                                             $.toast('Row Inserted')
                                             form.clear();
                                             form.hide();
                                             children_grid.load();

                                           }
                                       }
                                   });
                                   //console.log("im here now");
                                  }
                              }]
                              });


                        });

                      }
                    },{
                        type: 'button',
                        text: 'Delete',
                        action: 'remove'

                      }],

                      columns: [{
                          index: 'name',
                          title: 'Name',
                          type: 'combo',
                          data: {proxy: {
                            url: 'names.php'
                          }},
                          displayKey: 'name',
                            valueKey: 'key',
                        },{
                          index: 'sex',
                          title: 'Sex'
                        }, {
                          index: 'dob',
                          title: 'dob'
                        },{
                          type:'number',
                          index: 'age',
                          title: 'Age',
                          spin: true,
                          width: 60,
                          editable: false
                        }],
                          events:[{
                            update: function(grid){
                              //console.log("modificato");
                              //sconsole.log(selectedChild);
                              $.ajax({
                              url: 'src/children/update.php',
                                 type: 'POST',
                                 dataType: 'text',
                                 data: {
                                   child:JSON.stringify(selectedChild)
                                 },
                                 success: function(data) {
                                     console.log(data.data); // Inspect this in your console
                                     if(data.data == "success") {
                                       $.toast('Row Updated')
                                     }
                                 }, error: function (request, error) {
                                      alert("AJAX Call Error: " + error);
                                  }
                             });
                              //console.log(JSON.stringify(grid.store.data));
                            }
                          },{
                            remove: function(grid,id,item){
                              //console.log("removing " + item.data.id);
                              var request = $.ajax({
                                 url: 'src/children/delete.php',
                                 type: 'POST',
                                 data: {
                                   id:item.data.id
                                 },
                                 success: function(data) {
                                     console.log(data); // Inspect this in your console
                                      if(data.data == "success") {
                                        $.toast('Row Deleted')
                                      }
                                 },error: function (request, error) {
                                      alert("AJAX Call Error: " + error);
                                  }
                             });

                            }
                          },{
                            rowclick: function(grid, o){
                              selectedChild = o.data;
                              //console.log(selectedChild);
                            }
                          }





                        ]
                    });

                    for (var i = 0; i < obj.data.length; i++) {
                      children_grid.insert({

                        id: obj.data[i].id,
                        name: obj.data[i].name,
                        sex: obj.data[i].sex,
                        dob: obj.data[i].dob,
                        age: obj.data[i].age
                      });
                    }

                  }


    }
  }]

});

});

    </script>
</body>
</html>
