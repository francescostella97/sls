<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT * FROM `workplace`, `address` WHERE `workplace`.`id_area` = `address`.`id`';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('id' => $row['id'],'workplace_name' => $row['name'],
                     'workplace_hours' => $row['workplace_hours'],
                     'workplace_dresscode' => $row['dresscode'], 'workplace_manager' => $row['manager'],
                     'workplace_add1' => $row['add1'],'workplace_add2' => $row['add2'],
                     'workplace_area' => $row['area'],'workplace_county' => $row['county'],
                     'workplace_phone' => $row['phone']);
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
