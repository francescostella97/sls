<html>
<head>
    <title>AJAX PHP Search Engine Script for MySQL Database</title>

</head>
<body>
  <?php include '../commons/php/header.php'; ?>

  <div class="container-fluid">

    <div class="row">
      <div id="master" class="col-12" style="position:absolute;z-index:0">
      </div>

      <div id="form" class="col-2" style="position:absolute;z-index:1">
      </div>
    </div>

  </div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link href="http://code.fancygrid.com/fancy.min.css" rel="stylesheet">
<script src="http://code.fancygrid.com/fancy.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../commons/js/materialize.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../commons/js/materialize.min.js"></script>
<script src="../commons/js/typeahead.jquery.js"></script>
<script src="../commons/js/jquery.toast.js"></script>
<script src="../commons/js/jquery.toast.min.js"></script>
<link href="../commons/css/jquery.toast.css" rel="stylesheet" type="text/css">
<link href="../commons/css/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!--<link href="css/fancy.min.css" rel="stylesheet">
    <script src="js/fancy.min.js"></script>-->

<div id="container"></div>

    <script>
    $(document).ready(function(){
   $('.tooltipped').tooltip({delay: 50});
 });
    Fancy.enableCompo();


    function phoneInputFn(value) {
  value = parseInt(value.replace(/\-/g, ''));

  if (isNaN(value)) {
    value = '';
  } else {
    value = String(value);
  }

  switch (value.length) {
    case 0:
    case 1:
    case 2:
      break;
    case 4:
    case 5:
    case 6:
      value = value.replace(/^(\d{3})/, "$1-");
      break;
    case 7:
    case 8:
    case 9:
      value = value.replace(/^(\d{3})(\d{3})/, "$1-$2-");
      break;
    case 10:
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      break;
    default:
      value = value.substr(0, 10);
      value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
  }

  return value;
}

    function upInputFn(value) {
      value = value.toString().toLocaleUpperCase();

      return value;
    }
    var selectedObj
    var selectedChild
    String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
    $(function() {
      var obj



var main_grid = new FancyGrid({
  title: 'Students Database',
  renderTo: 'master',
  height: 600,
  theme: 'blue',
  selModel: 'rows',
  data: {
    proxy: {
      url: 'students_data.php'
    }
  },
  defaults: {
    type: 'string',
    width: 100,
    resizable: true,
    sortable: true,
    editable: true,
    filter: {
        header: true,
        emptyText: 'Search'
      }
  },
  paging: {
	pageSize: 20,
    pageSizeData: [5,10,20,50,100]
  },
  clicksToEdit: 2,
    rowEdit: true,
    tbar: [{
      type: 'button',
      text: 'Refresh',
      handler:function(){
        location.reload();
      }
    },{
  type: 'button',
  text: 'Add',
  handler: function(){
    //window.location.href = "http://stackoverflow.com";
    $(function() {
      var obj

          var form = new FancyForm({
          renderTo: 'form',
          title: {
          text: 'Register',
          tools : [{
                    text: 'Close',
                    handler: function() {
                      this.hide();
                    }
                  }]
          },
          draggable: true,
          width: 350,
          height: 400,
          tabs: ['General','Stay', 'Family','Type'],
              items: [  {type: 'tab',
                          items: [{
                            type: 'string',
                            label: 'Surname',
                            emptyText: 'Surname',
                            name: 'student_surname'
                          },{
                            type: 'combo',
                            label: 'Name',
                            emptyText: 'Name',
                            name: 'student_name',
                            data: {
                              proxy: {
                                url: 'names.php'
                              }
                            },
                              displayKey: 'name',
                              valueKey: 'key',
                          },{
                            type: 'date',
                            label: 'Dob',
                            emptyText: 'Date of birth',
                            name: 'student_dob',
                            format: {
                              read: 'Y-m-d',
                              write: 'Y-m-d',
                              edit:  'Y-m-d',
                            }
                          }]
                      },

                        {
                        type: 'tab',
                        label: 'Stay',
                        items: [{
                          type: 'date',
                          label: 'Arrival Date',
                          emptyText: 'Arrival Date',
                          name: 'arrival_date',
                          format: {
                            read: 'Y-m-d',
                            write: 'Y-m-d',
                            edit:  'Y-m-d',
                          }
                        }, {
                          type: 'number',
                          label: 'Arrival Time',
                          emptyText: 'Arrival Time (hh:mm)',
                          name: 'arrival_time',
                          format: {
                            write: 'H:i'
                          }
                        }, {
                          type: 'string',
                          label: 'Arrival Flight',
                          emptyText: 'Arrival Flight',
                          name: 'arrival_flight'
                        },{
                          type: 'date',
                          label: 'Departure Date',
                          emptyText: 'Departure Date',
                          name: 'departure_date',
                          format: {
                            read: 'Y-m-d',
                            write: 'Y-m-d',
                            edit:  'Y-m-d',
                          }
                        }, {
                          type: 'number',
                          label: 'Departure Time',
                          emptyText: 'Departure Time (hh:mm)',
                          name: 'departure_time',
                          format: {
                            write: 'H:i'
                          }
                        }, {
                          type: 'string',
                          label: 'Departure Flight',
                          emptyText: 'Departure Flight',
                          name: 'departure_flight'
                        }]
                      }
                      ,
                      {
                      type: 'tab',
                      label: 'Family',
                      items: [{
                        type: 'combo',
                        label: 'Host Family',
                        emptyText: 'Host Family',
                        name: 'family_id',
                        data: {
                          proxy: {
                            url: 'families.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'key',
                      }]//
                    },{
                      type: 'tab',
                      label: 'Type',
                      items: [{
                        name: 'type',
                        label: 'Type',
                        type: 'segbutton',
                        items: [{
                          text: 'Working Exp'
                        }, {
                          text: 'Closed'
                        }]
                      },{
                        type: 'combo',
                        label: 'Workplace',
                        emptyText: 'Workplace',
                        name: 'workplace_id',
                        data: {
                          proxy: {
                            url: 'workplaces.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'key',
                      },{
                        type: 'combo',
                        label: 'School',
                        emptyText: 'School',
                        name: 'school_id',
                        data: {
                          proxy: {
                            url: 'families.php'
                          }
                        },
                          displayKey: 'name',
                          valueKey: 'key',
                      }
                      ]
                    }

              ],
          buttons: ['side', {
          text: 'Clear',
          handler: function() {
            this.clear();
          }
          }, {
              text: 'Register',
              handler: function() {
                console.log(JSON.stringify(this.get()));
                $.ajax({
                   url: 'src/student/insert.php',
                   type: 'POST',
                   data: {
                     student:JSON.stringify(this.get())
                   },
                   success: function(data) {
                       console.log(data); // Inspect this in your console
                       if(data.data == "success") {
                         $.toast('Row Inserted')
                         form.clear();
                         form.hide();
                         main_grid.load();
                       }
                   }
               });
              }
          }]
          });


    });


  }
},{
    type: 'button',
    text: 'Delete',
    action: 'remove'
  }],
  columns: [{
    text: 'Student',
    columns: [{
      locked: true,
    index: 'student_id',
    title: 'Id',
    width: 40,
    editable: false
  },{
    locked: true,
    index: 'student_surname',
    title: 'Surname'
  },{
    index: 'student_name',
    title: 'Firstname',
    type: 'combo',
    data: {proxy: {
      url: 'names.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'student_dob',
    title: 'Dob',
    type: 'date',
    format: {
      read: 'Y-m-d',
      write: 'Y-m-d',
      edit:  'Y-m-d',
    }
  },{
    index: 'student_age',
    title: 'Age',
    width: 60,
    editable: false
  }]}, {
    text: 'Stay',
    columns: [{
    index: 'length',
    title: 'Length',
    width: 60,
    editable: false

  },{
    index: 'arrival_date',
    title: 'Arrival Date',
    type: 'date',
    format: {
      read: 'Y-m-d',
      write: 'Y-m-d',
      edit:  'Y-m-d',

    }
  },{
    index: 'arrival_time',
    title: 'Arrival Time'
  },{
    index: 'arrival_flight',
    title: 'Arrival Flight',

  },{
    index: 'departure_date',
    title: 'Departure Date',
    type: 'date',
    format: {
      read: 'Y-m-d',
      write: 'Y-m-d',
      edit:  'Y-m-d',

    }
  },{
    index: 'departure_time',
    title: 'Departure Time'
  },{
    index: 'departure_flight',
    title: 'Departure Flight'
    }]}
  ,   {
    text: 'Family',
    columns: [{
      index: 'family_surname',
      title: 'Family Name',
      type: 'combo',
      data: {proxy: {
        url: 'firsts.php'
      }},
      displayKey: 'name',
        valueKey: 'key',
    },{
      index: 'family_name',
      title: 'First',
      type: 'combo',
      data: {proxy: {
        url: 'names.php'
      }},
      displayKey: 'name',
        valueKey: 'key',
    },{
    index: 'no',
    title: 'No',
    width: 60,
    editable: false
  },{
    index: 'family_street',
    title: 'Street',
    editable: false
  },{
    index: 'family_area2',
    title: 'Add2',
    editable: false,

  },{
    index: 'family_area',
    title: 'Area',
    type: 'combo',
    data: {proxy: {
      url: 'areas.php'
    }},
    displayKey: 'area',
      valueKey: 'key',
  },{
    index: 'family_county',
    title: 'County',
    type: 'combo',
    data: {proxy: {
      url: 'counties.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'family_phone',
    title: 'Phone',
    editable: false
  },{
    index: 'family_mobile',
    title: 'Mobile',
    editable: false
  },{
    index: 'family_email',
    title: 'Email',
    editable: false
  },{
    type:'string',
    index: 'Confirmation Sent',
    title: 'Confirmation Sent'
  }]},
  {
    text: 'Workplace',
    columns: [{
      type:'combo',
    index: 'workplace_name',
    title: 'Workplace',
    data: {proxy: {
      url: 'workplaces.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'workplace_hours',
    title: 'Working Hours',
    editable: false,

  },{
    index: 'workplace_dresscode',
    title: 'Dresscode',
    editable: false,

  },{
    index: 'workplace_manager',
    title: 'Manager',
    editable: false,

  },{
    index: 'workplace_add1',
    title: 'Add1',
    editable: false,

  },{
    index: 'workplace_add2',
    title: 'Add2',
    editable: false,

  },{
    index: 'workplace_area',
    title: 'Area',
    type: 'combo',
    data: {proxy: {
      url: 'areas.php'
    }},
    displayKey: 'area',
      valueKey: 'key',
  },{
    index: 'workplace_county',
    title: 'County',
    type: 'combo',
    data: {proxy: {
      url: 'counties.php'
    }},
    displayKey: 'name',
      valueKey: 'key',
  },{
    index: 'workplace_phone',
    title: 'Phone',
    editable: false
  }]}
  ],
  events:[{
    update: function(grid){
      console.log("updating row " + JSON.stringify(selectedObj));
      $.ajax({
         url: 'src/student/update.php',
         type: 'POST',
         data: {
           student:JSON.stringify(selectedObj)
         },
         success: function(data) {
             console.log(data.data); // Inspect this in your console
             if(data.data == "success") $.toast('Row Updated')
         }
     });
      //console.log(JSON.stringify(grid.store.data));
    }
  },{
    remove: function(grid,id,item){
      console.log(item.data.student_id);
      $.ajax({
         url: 'src/student/delete.php',
         type: 'POST',
         data: {id:item.data.student_id},
         success: function(data) {
             console.log(data); // Inspect this in your console
              if(data.data == "success") $.toast('Row Deleted')
         }
     });
    }
  },{
    rowclick: function(grid, o){
      selectedObj = o.data;
    }
  }]

});

});

    </script>
</body>
</html>
