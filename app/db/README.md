# SLS's DB Application Doc

- [Intro](#intro)  
- [Navbar](#navbar)
- [Tables](#tables)  
- [Columns](#columns)
- [Host Family DB] (#host-family-db)
    - [Add a family row] (#add-a-family-row)
    - [Delete a family row] (#delete-a-family-row)
    - [Update a family row] (#update-a-family-row)
- [Stundent DB] (#student-db)
    - [Add a student row] (#add-a-student-row)
    - [Delete a student row] (#delete-a-student-row)
    - [Update a student row] (#update-a-student-row)
- [Workplace DB] (#workplace-db)  
    - [Add a workplace row] (#add-a-workplace-row)
    - [Delete a workplace row] (#delete-a-workplace-row)
    - [Update a workplace row] (#update-a-workplace-row)
  
# Intro
**SLS's DB Application** is a friendly interface that helps you displaying, adding and updating data.  
The home page of the application displays Host families' data and is show below:

![alt text](assets/db_home.PNG)

# Navbar  
The **Navbar**, located on the top right corner is used to access the main features of the whole Application (DB adn Email)  
From here you can:  
- Refresh the current page
- Jump to the Host Families Database page
- Jump to the Students Database page
- Jump to the Workplaces Database page
- Display Host Family Groups
- Write an email

![alt text](assets/navbar.PNG)

# Tables

![alt text](assets/db_head.PNG)  

Every table in the DB Application has three heading **buttons**:  
- Refresh -> Refresh the current page
- Add -> Opens a form for inserting a new row in the database
- Delete -> Deletes a row from the database  

# Columns
Every column can be stretched and used to order the data in an ascending or descending order by clicking on his name at the top.  
From the top of the column it is also possible to filter data by typing or selecting the filter from a combobox.

# Host Family DB
The host family db page includes two part, one displays the general family information and the other that lists the children of a selected family.  
Family's information include: **Name**, **Location**, **Parents Info**, **Pets Info** and **General Info** (Smoker, Availability, No. Children, Comment).  
When a family is selected (one click) the children table on the right is filled with the children information (Name, Sex, Dob and Age).  

## Add a family row
To add a new family to the database simply click the Add button on the Host Family Table Header.  
The form showed below would appear:

![alt text](assets/db_f_add.PNG)  

To add a new children to the database simply select one family from the right table and then click on the Add button on the Children Table Header.  
The form showed below would appear:

![alt text](assets/db_c_add.PNG)  
To save click 'Register'.  

## Delete a family row
To delete a row simply select one from a table and the click on the Delete Button on the Table Header.  

## Update a family row
To update a row double click on it, change data and the confirm the updates pressing the Update Button showed near the selected row.  

# Student DB
The student db has one table which includes: **Student Info**, **Stay Info**, **Family Info** and **Workplace Info**.

## Add a student row
To add a new student to the database simply click the Add button on the student Table Header.  
The form showed below would appear:

![alt text](assets/db_s_add.PNG)  
To save click 'Register'.  

## Delete a student row
To delete a row simply select one from a table and the click on the Delete Button on the Table Header.  

## Update a student row
To update a row double click on it, change data and the confirm the updates pressing the Update Button showed near the selected row.  


# Workplace DB
The workplace db has one table which includes: **Location Info**, **Wokring Hours**, **Manger Info** and **Dresscode**.

## Add a workplace row
To add a new workplace to the database simply click the Add button on the workplace Table Header.  
The form showed below would appear:

![alt text](assets/db_w_add.PNG)  
To save click 'Register'.  

## Delete a workplace row
To delete a row simply select one from a table and the click on the Delete Button on the Table Header.  

## Update a workplace row
To update a row double click on it, change data and the confirm the updates pressing the Update Button showed near the selected row.  


