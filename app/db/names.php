<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT * FROM `name` ORDER BY `name`.`name`  ASC LIMIT 20,100000';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('key' => $row['id'],'name' => $row['name']);
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
