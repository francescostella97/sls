<?php

include '../commons/php/db_connection.php';
$connection = OpenCon();
#echo "Connected Successfully";


#$connection = mysqli_connect("localhost", "username", "password", "library");
#$sql = "select name from family where title LIKE '%{$str}%'";
$sql = 'SELECT  student.id AS "student_id", student.surname AS "student_surname", name.name AS "student_name", student.dob AS "student_dob",
                stay.arrival_date, stay.arrival_time, stay.arrival_flight, stay.departure_date, stay.departure_time, stay.departure_flight,
                family.name AS "family_surname", family.wife_name AS "family_name", family.no, family.street AS "family_street", family.area2 AS "family_area2",
                address.area AS "family_area", address.county AS "family_county", family.phone AS "family_phone", family.wife_mobile AS "family_mobile", family.email AS "family_email",
                workplace.name AS "workplace_name", workplace.working_hours AS "workplace_hours", workplace.dresscode AS "workplace_dresscode",
                workplace.manager AS "workplace_manager", workplace.add1 AS "workplace_add1", workplace.add2 AS "workplace_add2", workplace.phone AS "workplace_phone"
               FROM `student`, `family`, `name`,`stay`,`address`, `workplace` WHERE `student`.`id_family` = `family`.`id` AND `student`.`id_name` = `name`.`id` AND `stay`.`id_student` = `student`.id AND address.id = family.id_address';
#echo $sql;
$result = mysqli_query($connection, $sql);

$array = array();
while($row = mysqli_fetch_assoc($result)) {

  $query_workplace = "SELECT `address`.`area`, `address`.`county` FROM `address`, `workplace`, student WHERE workplace.id = student.id_workplace AND workplace.id_area = address.Id AND student.id = ".$row['student_id'];
  $result_workplace = mysqli_query($connection, $query_workplace);
  $row_workplace = mysqli_fetch_assoc($result_workplace);


  $from = new DateTime($row['student_dob']);
  $to   = new DateTime('today');
  $age = $from->diff($to)->y;

  $from = new DateTime($row['departure_date']);
  $to   = new DateTime($row['arrival_date']);
  $length = $from->diff($to)->days;

    $array[] = array('student_id' => $row['student_id'], 'student_surname' => $row['student_surname'],'student_name' => $row['student_name'],'student_dob' => $row['student_dob'],'student_age' => $age,
                     'arrival_date' => $row['arrival_date'], 'arrival_time' => $row['arrival_time'], 'arrival_flight' => $row['arrival_flight'],
                     'length' => $length, 'departure_date' => $row['departure_date'], 'departure_time' => $row['departure_time'],'departure_flight' => $row['departure_flight'],
                     'family_surname' => $row['family_surname'],'family_name' => $row['family_name'],
                     'no' => $row['no'],'family_street' => $row['family_street'],'family_area2' => $row['family_area2'],
                     'family_area' => $row['family_area'],'family_county' => $row['family_county'],
                     'family_phone' => $row['family_phone'],'family_mobile' => $row['family_mobile'],'family_email' => $row['family_email'],

                     'workplace_name' => $row['workplace_name'],'workplace_hours' => $row['workplace_hours'],
                     'workplace_dresscode' => $row['workplace_dresscode'],'workplace_manager' => $row['workplace_manager'],
                     'workplace_add1' => $row['workplace_add1'],'workplace_add2' => $row['workplace_add2'],'workplace_area' => $row_workplace['area'],'workplace_county' =>$row_workplace['county'],'workplace_phone' => $row['workplace_phone']);
}

header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
