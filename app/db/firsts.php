<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT id,name FROM `family` ORDER BY `family`.`name`';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('key' => $row['id'],'name' => $row['name']);
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
