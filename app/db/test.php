<html>
  <head>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="../commons/js/jquery.easy-autocomplete.min.js"></script>
    <link rel="stylesheet" href="../commons/css/easy-autocomplete.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../commons/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../commons/css/style.css"/>


<script>
$(document).ready(function(){
  var json = (function () {
       var json = null;
       $.ajax({
           'async': false,
           'global': false,
           'url': '../email/emails.json',
           'dataType': "json",
           'success': function (data) {
               json = data;
           }
       });
       return json;
   })();
   var labels = [];

   //loop thru all the properties you care about
   json.forEach(function(obj) {
     labels.push(obj.label)

   });
   console.log(labels);
   var options = {

	data: labels

};
  $("#provider-remote").easyAutocomplete(options);
});


        </script>
  </head>
  <body>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../commons/js/materialize.min.js"></script>
    <script>


       function insert(text){
         $('textarea.tagging').append(" " + text + " ");//not sure what to use here to "know" where cursor currently is
         $('#tagging').addClass('active')
       }
         $(document).ready(function() {


         $('.tooltipped').tooltip({delay: 50});
         $('select').material_select();
         $('input#input_text, textarea#object').characterCounter();

         $('input#input_text, textarea#textarea1').characterCounter();
      });


    </script>


     <?php include '../commons/php/header.php'; ?>
 <div class="row">

   <form class="col s12" style="padding-top:25px;">



         <?php
           include '../commons/php/db_connection.php';
           $connection = OpenCon();

           $sql = 'SELECT email FROM `family` WHERE email != "" ORDER BY `family`.`email` ASC';
           $result = mysqli_query($connection, $sql);
           $array = array();
           print '<div class="row col s6"> <div class="input-field col s12">
             <select  multiple>
               <option value="" disabled selected>Choose your option</option>';
           while($row = mysqli_fetch_assoc($result)) {
               //$array[] = array('key' => $row['id'],'name' => $row['email']);
               print '<option value="'.$row['id'].'">'.$row['email'].'</option>';
           }
           print '</select>

             <label>Choose emails</label>
           </div></div>';

           $sql = 'SELECT name FROM `hfgroup` ORDER BY `hfgroup`.`name` ASC';
           $result = mysqli_query($connection, $sql);
           $array = array();
           print '<div class="row col s6"> <div class="input-field col s12">
             <select multiple>
               <option value="" disabled selected>Choose your option</option>';
           while($row = mysqli_fetch_assoc($result)) {
               //$array[] = array('key' => $row['id'],'name' => $row['email']);
               print '<option value="'.$row['id'].'">'.$row['name'].'</option>';
           }
           print '</select>

             <label>Choose group</label>
           </div></div>';


         ?>

         <input id="provider-remote" />

         <div class="row">
           <div class="input-field col s12">
             <textarea id="object" class="materialize-textarea" data-length="50"></textarea>
             <label for="object">Object</label>
           </div>
         </div>

     <div class="row">
       <div class="input-field col s12">
         <textarea id="textarea1" class="materialize-textarea tagging" rows="10" data-length="300"></textarea>
         <label for="textarea1" id="tagging">Body</label>
       </div>
     </div>

     <div class="row" style="
         position: fixed;
         left: 23px;
         bottom: 23px;
         padding-top: 15px;
         margin-bottom: 0;
         z-index: 997
     ">

       <div onclick="insert('Dear Student')" class="chip"><a>Dear Student</a></div>
       <div onclick="insert('Dear Family')" class="chip"><a>Dear Family</a></div>

     </div>

     <div class="fixed-action-btn">
       <button class="btn waves-effect waves-light" type="submit" name="action">Send
        <i class="material-icons right">send</i>
      </button>

     </div>




   </form>
 </div>

  </body>
</hmtl>
