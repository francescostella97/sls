<?php
include '../commons/php/db_connection.php';
$connection = OpenCon();
$sql = 'SELECT * FROM `address` ORDER BY `address`.`county` ';
$result = mysqli_query($connection, $sql);
$array = array();
while($row = mysqli_fetch_assoc($result)) {
    $array[] = array('key' => $row['Id'],'name' => $row['county']);
}
header('Content-type: application/json');
echo json_encode(array('data' => $array));
?>
